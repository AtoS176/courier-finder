package pl.poweska.parser.locationCreator;

import org.jsoup.nodes.Element;
import pl.poweska.domain.Address;
import pl.poweska.domain.Company;
import pl.poweska.domain.Location;

import java.util.Arrays;
import java.util.List;

public class UpsLocationCreator implements LocationCreator<Element> {
    @Override
    public Location createLocation(Element element) {
        String name = element.select("h2").first().text();
        String data = element.select("p").html();

        String text = data
                .replace("<br>", "\n")
                .replace("ul.", "")
                .replace("</b>", "")
                .replace("</p>", "")
                .replace("<strong>", "")
                .replace("</strong>", "")
                .replace("ul.", "")
                .replace("Ul.", "");

        String[] lines = text.split("\n");
        List<String> cityAddress = splitCityAddress(lines[1]);

        Address address = Address.builder()
                .city(cityAddress.get(1))
                .postCode(cityAddress.get(0))
                .street(lines[0])
                .build();

        return new Location(name, address, Company.UPC);
    }

    private List<String> splitCityAddress(String lineAddress){
        String[] splitAddress = lineAddress.split(" ");

        StringBuilder city = new StringBuilder();
        for(int i = 1; i < splitAddress.length; i++){
            city.append(splitAddress[i]);
            city.append(" ");
        }

        return Arrays.asList(splitAddress[0], city.toString());
    }

}
