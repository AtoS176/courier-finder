package pl.poweska.parser.locationCreator;

import pl.poweska.domain.Location;

public interface LocationCreator <T> {
    Location createLocation(T t);
}
