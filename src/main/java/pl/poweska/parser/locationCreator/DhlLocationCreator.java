package pl.poweska.parser.locationCreator;

import org.jsoup.nodes.Element;
import pl.poweska.domain.Address;
import pl.poweska.domain.Company;
import pl.poweska.domain.Location;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DhlLocationCreator implements LocationCreator<Element> {
    private final String postCodePattern = "[0-9]{2}[ ]*[-][ ]*[0-9]{3}";
    private final String dataClassName = "panel-collapse collapse";
    private final String titleClassName = "panel-title";
    private final String textClassName = "textarea";

    @Override
    public Location createLocation(Element element) {
        Element title = element.getElementsByClass(titleClassName).first();
        Element dataElement = element.getElementsByClass(dataClassName).first();
        Element textElement = dataElement.getElementsByClass(textClassName).first();

        String text = textElement.html()
                .replace("<br>", "\n")
                .replace("ul.", "")
                .replace("</b>", "")
                .replace("</p>", "");

        String[] lines = text.split("\n");
        String fullStreet = lines[1];
        String[] splitStreet = fullStreet.split(",");

        String fullCity;
        if (splitStreet.length == 1) {
            int cityLines = lines[2].charAt(1) == '(' ? 3 : 2;
            fullCity = lines[cityLines];
        } else {
            fullCity = splitStreet[1];
        }

        String[] splitCity = fullCity.split(postCodePattern);
        Pattern p = Pattern.compile(postCodePattern);
        Matcher matcher = p.matcher(fullCity);

        if(!matcher.find()){
            throw new IllegalArgumentException();
        }

        Address address = Address.builder()
                .city(splitCity[1])
                .postCode(matcher.group())
                .street(splitStreet[0])
                .build();

        return new Location(title.text(), address, Company.DHL);
    }

}
