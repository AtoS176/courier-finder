package pl.poweska.parser.locationCreator;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pl.poweska.domain.Address;
import pl.poweska.domain.Company;
import pl.poweska.domain.Location;

public class DpdLocationCreator implements LocationCreator<Elements> {
    @Override
    public Location createLocation(Elements column) {
        String name = column.get(0).text();
        Address address = convertColumnToAddress(column.get(1));
        return new Location(name, address, Company.DPD);
    }

    private Address convertColumnToAddress(Element column){
        String[] address = column.text().split(",");
        String streetAddress = address[0].replaceAll("ul. ", "");
        String cityAddress = address[1].substring(1);

        int citySpace = findSpace(cityAddress);

        return Address.builder()
                .postCode(cityAddress.substring(0, citySpace))
                .city(cityAddress.substring(citySpace+1))
                .street(streetAddress)
                .build();
    }

    private int findSpace(String address){
        for(int i = 0; i < address.length(); i++){
            if(address.charAt(i) == ' ') {
                return i;
            }
        }
        throw new IllegalArgumentException();
    }

}