package pl.poweska.parser.pageParser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import pl.poweska.domain.Location;

import java.io.IOException;
import java.util.List;

public abstract class HtmlParser {
    protected final String url;
    protected final String divId;

    protected HtmlParser(String url, String divId) {
        this.url = url;
        this.divId = divId;
    }

    protected abstract List<Location> createLocation(Element mainDiv);

    public List<Location> parse() throws IOException {
        Document document = Jsoup.connect(url).get();
        Element div = document.getElementById(divId);
        return createLocation(div);
    }

}
