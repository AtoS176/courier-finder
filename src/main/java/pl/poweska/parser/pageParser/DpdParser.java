package pl.poweska.parser.pageParser;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import pl.poweska.domain.Location;
import pl.poweska.parser.locationCreator.DpdLocationCreator;
import pl.poweska.parser.locationCreator.LocationCreator;

import java.util.ArrayList;
import java.util.List;

@Component
public class DpdParser extends HtmlParser {
    private final LocationCreator<Elements> locationCreator;

    public DpdParser() {
      super("https://www.dpd.com/pl/pl/o-dpd/kontakt/oddzialy-dpd-polska/", "supsystic-table-7156");
      locationCreator = new DpdLocationCreator();
    }

    @Override
    protected List<Location> createLocation(Element mainDiv) {
        List<Location> dpdLocations = new ArrayList<>();
        Elements rows = mainDiv.select("tr");

        for(int i = 2; i < rows.size(); i++){
            Elements columns = rows.get(i).select("td");
            dpdLocations.add(locationCreator.createLocation(columns));
        }

        return dpdLocations;
    }

}
