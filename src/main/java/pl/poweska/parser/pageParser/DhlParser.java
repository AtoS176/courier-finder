package pl.poweska.parser.pageParser;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import pl.poweska.domain.Location;
import pl.poweska.parser.locationCreator.DhlLocationCreator;
import pl.poweska.parser.locationCreator.LocationCreator;

import java.util.ArrayList;
import java.util.List;

@Component
public class DhlParser extends HtmlParser {
    private final String className;
    private final LocationCreator<Element> locationCreator;

    public DhlParser() {
        super("https://www.dhlparcel.pl/pl/dla-biznesu/uslugi/punkty-dhl/pok.html", "accordion");
        className = "accordioncontent panel panel-default dhl redesign";
        locationCreator = new DhlLocationCreator();
    }

    @Override
    protected List<Location> createLocation(Element mainDiv) {
        List<Location> dhlLocations = new ArrayList<>();
        Elements data = mainDiv.getElementsByClass(className);

        for(Element element : data){
            dhlLocations.add(locationCreator.createLocation(element));
        }

        return dhlLocations;
    }

}
