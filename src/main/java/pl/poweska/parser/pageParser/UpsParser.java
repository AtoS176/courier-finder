package pl.poweska.parser.pageParser;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import pl.poweska.domain.Location;
import pl.poweska.parser.locationCreator.LocationCreator;
import pl.poweska.parser.locationCreator.UpsLocationCreator;

import java.util.ArrayList;
import java.util.List;

@Component
public class UpsParser extends HtmlParser {
    private final String className;
    private final LocationCreator<Element> locationCreator;

    public UpsParser() {
        super("https://www.ups.com/pl/pl/shipping/dropoff.page", "sectioniov5pt7a");
        className = "ups-content_with_anchor-wrapper";
        locationCreator = new UpsLocationCreator();
    }

    @Override
    protected List<Location> createLocation(Element mainDiv) {
        List<Location> upcLocations = new ArrayList<>();
        Elements data = mainDiv.getElementsByClass(className);

        for(Element element : data){
            upcLocations.add(locationCreator.createLocation(element));
        }

        return upcLocations;
    }

}
