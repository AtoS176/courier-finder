package pl.poweska.domain;

public class Location {
    private final String name;
    private final Address address;
    private final Company company;

    public Location(String name, Address address, Company company) {
        this.name = name;
        this.address = address;
        this.company = company;
    }

    @Override
    public String toString() {
        return "Firma: " + company.toString() + "\n" +
                "Nazwa Placówki: " + name + "\n" +
                address.toString();
    }

}
