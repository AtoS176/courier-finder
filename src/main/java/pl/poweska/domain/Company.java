package pl.poweska.domain;

public enum Company {
    DHL,
    DPD,
    UPC
}
