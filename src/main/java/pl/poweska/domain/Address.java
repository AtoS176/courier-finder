package pl.poweska.domain;

import lombok.Builder;

@Builder
public class Address {
    private final String city;
    private final String postCode;
    private final String street;

    public Address(String city, String postCode, String street) {
        this.city = city;
        this.postCode = postCode;
        this.street = street;
    }

    @Override
    public String toString() {
        return "Kod Pocztowy : " + postCode + System.lineSeparator() +
                "Miasto : " + city + System.lineSeparator() +
                "Ulica : " + street;
    }
}
